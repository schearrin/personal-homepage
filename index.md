---
layout: home
title: Accueil
excerpt: "Sophie Cheang, étudiante en MIAGE à l'Université Paris-Saclay"
---

Bonjour et bienvenue sur mon site personnel !

Je m’appelle Sophie Cheang, je suis passionnée par tout ce qui touche à l’informatique et aux nouvelles technologies.

Je suis étudiante en Master MIAGE (Méthodes Informatiques Appliquées à la Gestion des Entreprises) à l’université Paris-Saclay. Je suis titulaire d'une licence Informatique parcours MIAGE ainsi que d'un DUT STID (Statistique et Informatique Décisionnelle).

La MIAGE est une formation à finalité professionnelle qui permet aux étudiants de se positionner comme des concepteurs, des prescripteurs ou des décideurs utilisateurs des Nouvelles Technologies de l'Information et de la Communication, opérant comme des responsables privilégiés entre le monde de la Gestion et celui de l'informatique.

Ce site a pour but de me faire connaître et de vous faire partager mes réalisations ainsi que mes différents travaux réalisés au cours de mon cursus.

Bonne visite !