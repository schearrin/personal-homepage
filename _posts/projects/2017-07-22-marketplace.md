---

layout: post
title: Marketplace - Vente à distance
excerpt: "De Décembre 2016 à janvier 2017 : Réalisation d'un site web d'une société de vente à distance de médias."
categories: ['projects', 'info']
projects: informatique
tags: [postgresql, javaee/j2ee, jsp, html, xml, git, tomcat, eclipse, css, mvc, xslt, dom, saxon]

---

### Objectif

L'objectif de ce projet est de réaliser un site web d'une société de vente à distance de médias.

* Fonctionnalités

Nous avons tout d'abord implémenté les fonctionnalités de bases pour les différents rôles (visiteur, client, administrateur) telles que l'inscription, la consultation et l'ajout, la modification ainsi que la suppression de clients/produits.

Puis nous avons ajouté des fonctionnalités un peu plus complexes telles que la suggestion d'une liste de produits selon certains critères (même catégories, mots-clés, ...), une fonctionnalité de recherche permettant de regrouper plusieurs critères et la possibilité de générer deux PDF à partir des données du site (un qui décrit l'ensemble des produits proposés et un autre qui regroupe la liste des clients avec leurs informations et le chiffre d'affaire réalisé depuis une période donnée).

Pour finir, nous avons implémenté une fonctionnalité de collaboration en utilisant des Web Services permettant ainsi de pouvoir exporter le catalogue de produit en XML et d'importer un catalogue de produit d'un autre vendeur sur le site tout en redirigeant vers le site du vrai vendeur.

* Rapport

Ce projet a abouti par la rédaction d'un rapport indiquant les choix de conceptions (découpage des données, XML, BD, DTD et schéma des bases), d'implémentation (langages utilisés et exemples de codes pertinents), la description des scénarios pour chaque rôle (visiteur, client, administrateur) et la répartition du travail au sein du groupe.

### Quelques screenshots

<figure class="third">
	<a href="/images/marketplace/marketplace1.jpg"><img src="/images/marketplace/marketplace1.jpg"></a>
	<a href="/images/marketplace/marketplace2.jpg"><img src="/images/marketplace/marketplace2.jpg"></a>
	<a href="/images/marketplace/marketplace3.jpg"><img src="/images/marketplace/marketplace3.jpg"></a>
</figure>

### Outils et langages
