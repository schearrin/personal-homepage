---

layout: post
title: Code Futurama - Modélisation et animation 3D
excerpt: "De février 2013 à mai 2013 : Réalisation 3D résumant les techniques de modélisation et d'animation 3D apprises."
categories: ['projects', 'perso']
projects: personnel
tags: [3dsmax, sony vegas]

---

### Objectif

<figure class="third">
	<a href="/images/animation3d/3d7.JPG"><img src="/images/animation3d/3d7.JPG"></a>
	<a href="/images/animation3d/3d8.JPG"><img src="/images/animation3d/3d8.JPG"></a>
	<a href="/images/animation3d/3d9.JPG"><img src="/images/animation3d/3d9.JPG"></a>
</figure>

L'objectif de ce projet est de résumer les techniques de modélisation et d'animation 3D apprises à travers une courte vidéo.

Les activités réalisées pour ce projet ont été les suivantes :

* Story-board

Dans un premier temps, j'ai réalisé une story-board expliquant en plusieurs séquences l'histoire de la vidéo à réaliser.

* Modélisation

J'ai ensuite modélisé les personnages et les décors à travers différents techniques de modélisation qui consiste à utiliser des formes (boîte, cylindre, polygone, sphère, plan, ...) puis de les éditer afin d'avoir une nouvelle forme.

<figure class="half">
	<a href="/images/animation3d/3d1.JPG"><img src="/images/animation3d/3d1.JPG"></a>
	<a href="/images/animation3d/3d2.JPG"><img src="/images/animation3d/3d2.JPG"></a>
</figure>

* Animation

L'idée est ensuite d'animer les objets modélisés à travers des techniques d'animations. Les plus connus étant les suivantes :

-- Le Keyframing qui consiste à définir à un objet des propriétés à des moments clés dans le temps.

-- Le Path constraint qui consiste à restreindre le chemin d'un objet tout le long d'une "spline" (ligne, rectangle, cercles, etc.).

-- Le Footstep qui consiste à poser des pas (marche, course, saut) à partir du squelette du personnage créé.

-- L'animation physique qui permet d'animer des corps rigides, déformables, souples ou encore des tissus lorsque ces derniers entrent en collisions à partir d'un module "Mass FX". Ce qui permet de provoquer une interaction des forces.

<figure class="half">
	<a href="/images/animation3d/3d3.JPG"><img src="/images/animation3d/3d3.JPG"></a>
	<a href="/images/animation3d/3d4.JPG"><img src="/images/animation3d/3d4.JPG"></a>
</figure>
<figure class="half">
	<a href="/images/animation3d/3d5.JPG"><img src="/images/animation3d/3d5.JPG"></a>
	<a href="/images/animation3d/3d6.JPG"><img src="/images/animation3d/3d6.JPG"></a>
</figure>


* Rendu

Pour finir, un rendu des séquences image par image a été fait au lieu d'un rendu vidéo classique qui a permis de faire un gain de temps considérable. Les images ont été assemblé via l'outil "Jeu de RAM" dans 3DSMax afin de créer des vidéos puis un montage vidéo a été fait pour illustrer la Story-board.

### Outils