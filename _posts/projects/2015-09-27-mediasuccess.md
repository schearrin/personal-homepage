---

layout: post
title: Mediasuccess - Gestion d'une médiathèque
excerpt: "De janvier 2015 à mai 2015 : Projet Génie Logiciel. Mediasuccess, un système de gestion en ligne d'une médiathèque."
categories: ['projects', 'info']
projects: informatique
tags: [css, html, php, sql, bitbucket, uml, wamp, papyrus, innodb, bootstrap template]

---

<figure class="half">
	<a href="/images/mediasuccess/mediasuccess.jpg"><img src="/images/mediasuccess/mediasuccess.jpg"></a>
	<a href="/images/mediasuccess/mediasuccess2.jpg"><img src="/images/mediasuccess/mediasuccess2.jpg"></a>
</figure>

### Objectif

L'objectif de ce projet a été de modéliser un système de gestion d'une médiathèque à travers la mise en oeuvre des méthodes de Génie Logiciel. Le travail a été effectué en 3 grandes étapes : 

* Analyse

Dans une première étape, nous analysons les besoins, la faisabilité et collectionnons les données permettant à la réalisation du système à travers la rédaction d'un cahier des charges : description du contexte, les utilisateurs, spécifications, fonctionnalités, restrictions, l'aspect final et les diagrammes uml. 

* Conception

Dans une deuxième étape, nous déterminons les outils et mécanismes du système à travers la rédaction du document de conception : choix technologiques, composants, architecture du système, conception raffinée et implémentation.

* Codage

La dernière étape de notre projet se résume par la création détaillée de notre travail suivant les fonctionnalités définies à travers : programmations, tests, vérifications et débogages. Un logiciel de gestion de version a été utilisé lors de cette étape pour permettre au mieux d'organiser et de gérer notre projet de groupe.


### Résultat final

<figure class="third">
	<a href="/images/mediasuccess/contact.jpg"><img src="/images/mediasuccess/contact.jpg"></a>
	<a href="/images/mediasuccess/bd.jpg"><img src="/images/mediasuccess/bd.jpg"></a>
	<a href="/images/mediasuccess/rechercher.jpg"><img src="/images/mediasuccess/rechercher.jpg"></a>
</figure>
<figure class="third">
	<a href="/images/mediasuccess/adherent.jpg"><img src="/images/mediasuccess/adherent.jpg"></a>
	<a href="/images/mediasuccess/creercompte.jpg"><img src="/images/mediasuccess/creercompte.jpg"></a>
	<a href="/images/mediasuccess/connexionreussie.jpg"><img src="/images/mediasuccess/connexionreussie.jpg"></a>
</figure>
<figure class="third">
	<a href="/images/mediasuccess/compteadherent.jpg"><img src="/images/mediasuccess/compteadherent.jpg"></a>
	<a href="/images/mediasuccess/reserver.jpg"><img src="/images/mediasuccess/reserver.jpg"></a>
	<a href="/images/mediasuccess/comptegestionnaire.jpg"><img src="/images/mediasuccess/comptegestionnaire.jpg"></a>
</figure>

### Outils et langages

