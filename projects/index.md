---
layout: page
title: Mes réalisations
excerpt: "An archive of projects sorted by categories and date."
search_omit: true
---

{% if site.categories['info'] %}
##### Développement & SI

<ul class="post-list">
{% for post in site.categories['info'] %} 
  <li><article><a href="{{ site.url }}{{ post.url }}">{{ post.title }} <span class="entry-date">Publié le <time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%d-%m-%Y" }}</time></span>{% if post.excerpt %} <span class="excerpt">{{ post.excerpt }}</span>{% endif %}</a></article></li>
{% endfor %}
</ul>
{% endif %}

{% if site.categories['stat'] %}
##### Big Data & Statistiques

<ul class="post-list">
{% for post in site.categories['stat'] %} 
  <li><article><a href="{{ site.url }}{{ post.url }}">{{ post.title }} <span class="entry-date">Publié le <time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%d-%m-%Y" }}</time></span>{% if post.excerpt %} <span class="excerpt">{{ post.excerpt }}</span>{% endif %}</a></article></li>
{% endfor %}
</ul>
{% endif %}


{% if site.categories['perso'] %}
##### Personnelles

<ul class="post-list">
{% for post in site.categories['perso'] %} 
  <li><article><a href="{{ site.url }}{{ post.url }}">{{ post.title }} <span class="entry-date">Publié le <time datetime="{{ post.date | date_to_xmlschema }}">{{ post.date | date: "%d-%m-%Y" }}</time></span>{% if post.excerpt %} <span class="excerpt">{{ post.excerpt }}</span>{% endif %}</a></article></li>
{% endfor %}
</ul>
{% endif %}